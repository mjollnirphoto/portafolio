# **_ PORTFOLIO _**

Partiendo de la base que tenemos en programación 💻 ( **Html, CSS, JavaScript** ) he realizado un portfolio de todos mis conocimientos y de todos mis trabajos realizados hasta la fecha..

---

✅ Para ello he creado un `index.html` lo más correcto posible, con una estructura sencilla pero con los elementos más importantes que podemos encontrarnos en una calculadora básica. Hemos creado algunas etiquetas <meta> que consideramos de relativa importancia ( _author, description, keywords ..._ ).

✅ El siguiente paso fue ponerse con el archivo `script.js` que será el encargado de realizar todo lo necesario.

✅ Y para finalizar, he realizado el diseño , gracias al documento `style.css` le doy una imagen visual a la creación, tratando de proporcionarle un aspecto interesante ( añadiendo algun pequeña animación ... ) y que no dañe demasiado la vista 👀 .

![Pequeña Imagen de creación](https://media.hswstatic.com/eyJidWNrZXQiOiJjb250ZW50Lmhzd3N0YXRpYy5jb20iLCJrZXkiOiJnaWZcL3VwZGF0ZS0tLUZyYW5rZW5zdGVpbnMtbW9uc3Rlci5qcGciLCJlZGl0cyI6eyJyZXNpemUiOnsid2lkdGgiOjgyOH19fQ==)

---

> " La manera de comenzar es dejar de hablar de ello y comenzar a hacerlo. " — Walt Disney

---

### **Autores ✒️ :**

Todo el trabajo ha sido creado por :

- <span style="color:green">Marcos</span> : [LinkedIn](https://es.linkedin.com/in/marcos-v%C3%A1zquez-gonz%C3%A1lez-44379562?trk=people-guest_people_search-card)
  ➡️ Creador de la estructura base **Html** , **JavaScript** y de diseñar la parte visual de **CSS**.

---

### **ENLACE :**

[LinkedIn](https://portafolio-xi-weld.vercel.app/)
